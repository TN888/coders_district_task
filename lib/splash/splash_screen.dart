import 'package:coders_district_task/global/localization/localization.dart';
import 'package:coders_district_task/global/routes/routes_names.dart';
import 'package:coders_district_task/global/utils/font_sizes.dart';
import 'package:coders_district_task/global/utils/text_styles.dart';
import 'package:coders_district_task/global/utils/utils.dart';
import 'package:coders_district_task/home/screen/home_screen.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(milliseconds: 3500)).then(
      (_) {
        Navigator.of(context).pushReplacementNamed(home);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    setStatusBarColor(Colors.transparent, Brightness.light);
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/background_image.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/icons/logo.png'),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(
                getTranslate(context, 'company_name'),
                style: TextStyles.getWhiteBold(FontSizes.title),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
