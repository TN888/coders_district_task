import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:coders_district_task/global/apis/language_api.dart';
import 'package:coders_district_task/global/localization/localization.dart';
import 'package:coders_district_task/home/careers_model.dart';
import 'package:coders_district_task/home/home_api.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit({
    @required this.homeApi,
  })  : assert(homeApi != null),
        super(HomeInitial());

  final HomeApi homeApi;

  getCareers(BuildContext context, {String language = ENGLISH}) {
    emit(HomeLoading());

    // Future.delayed(Duration(milliseconds: 3000)).then((value) => emit(HomeError(getTranslate(context, 'try_again'))));

    homeApi.getCareers(context, language).then(
      (response) {
        if (response.statusCode == 200) {
          var careersModel = CareersModel.fromJson(json.decode(response.body));
          emit(HomeSuccess(careersModel, language));
        } else {
          emit(HomeError(getTranslate(context, 'try_again')));
        }
      },
    ).catchError(
      (error) {
        emit(HomeError(getTranslate(context, 'try_again')));
      },
    );
  }
}
