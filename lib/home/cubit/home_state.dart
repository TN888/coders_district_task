part of 'home_cubit.dart';

abstract class HomeState extends Equatable {
  const HomeState();
}

class HomeInitial extends HomeState {
  @override
  List<Object> get props => [];
}

class HomeSuccess extends HomeState {
  HomeSuccess(this.careersModel, this.language);

  final CareersModel careersModel;
  final String language;

  @override
  List<Object> get props => [careersModel, language];
}

class HomeLoading extends HomeState {
  @override
  List<Object> get props => [];
}

class HomeError extends HomeState {
  HomeError(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}
