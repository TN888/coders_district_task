import 'package:coders_district_task/global/apis/language_api.dart';
import 'package:coders_district_task/home/cubit/home_cubit.dart';
import 'package:coders_district_task/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_switch/flutter_switch.dart';

//If activated --> English
//Not activated --> Arabic

class LanguageSwitchButton extends StatefulWidget {
  const LanguageSwitchButton({
    Key key,
    @required this.isActivated,
  })  : assert(isActivated != null),
        super(key: key);

  final bool isActivated;

  @override
  _LanguageSwitchButtonState createState() => _LanguageSwitchButtonState();
}

class _LanguageSwitchButtonState extends State<LanguageSwitchButton> {
  LanguageApi languageApi;

  HomeCubit homeCubit;
  bool isActivated;

  @override
  void initState() {
    super.initState();

    languageApi = RepositoryProvider.of<LanguageApi>(context);

    homeCubit = BlocProvider.of<HomeCubit>(context);
    isActivated = widget.isActivated;
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomeCubit, HomeState>(
      listener: (context, state) {
        if (state is HomeSuccess) {
          languageApi.setLanguage(state.language).then(
                (value) => streamController.add(true),
              );
        } else if (state is HomeError) {
          setState(() {});
        }
      },
      builder: (context, state) {
        if (state is HomeSuccess) {
          isActivated = state.language == ENGLISH;
        } else if (state is HomeError) {
          isActivated = widget.isActivated;
        }
        return FlutterSwitch(
          width: MediaQuery.of(context).size.width * 0.16,
          height: (MediaQuery.of(context).size.width * 0.07),
          value: isActivated,
          activeText: ENGLISH,
          showOnOff: true,
          switchBorder: Border.all(color: Colors.white),
          activeColor: Colors.grey,
          valueFontSize: 12,
          inactiveText: ARABIC,
          onToggle: (isNewlyActivated) {
            setState(() {
              isActivated = isNewlyActivated;
            });
            homeCubit.getCareers(context, language: isActivated ? ENGLISH : ARABIC);
          },
        );
      },
    );
  }
}
