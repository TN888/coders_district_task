import 'package:coders_district_task/global/utils/font_sizes.dart';
import 'package:flutter/material.dart';

class GridViewTile extends StatelessWidget {
  const GridViewTile({
    Key key,
    @required this.text,
    this.color,
  })  : assert(text != null),
        super(key: key);

  final String text;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: Center(
        child: Text(
          text,
          style: TextStyle(
            fontSize: FontSizes.title + FontSizes.text,
            fontWeight: FontWeight.bold,
            color: color,
          ),
        ),
      ),
    );
  }
}
