import 'dart:math';

import 'package:coders_district_task/global/apis/language_api.dart';
import 'package:coders_district_task/global/localization/localization.dart';
import 'package:coders_district_task/global/utils/utils.dart';
import 'package:coders_district_task/home/cubit/home_cubit.dart';
import 'package:coders_district_task/home/screen/grid_view_tile.dart';
import 'package:coders_district_task/home/screen/language_switch_button.dart';
import 'package:coders_district_task/home/screen/video_player_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'action_button.dart';
import 'my_scroll_behavior.dart';

const List<Color> _letters_colors = [
  Color(0xFF0e5f58),
  Color(0xFF6fc989),
  Color(0xFF00a49a),
  Color(0xFF007a82),
];

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  LanguageApi languageApi;
  HomeCubit homeCubit;

  @override
  void initState() {
    super.initState();

    languageApi = RepositoryProvider.of<LanguageApi>(context);

    homeCubit = BlocProvider.of<HomeCubit>(context);
    homeCubit.getCareers(context, language: languageApi.language);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: buildAppBar(),
      body: buildBody(),
    );
  }

  Widget buildBody() {
    return Container(
      padding: EdgeInsets.only(
          left: MediaQuery.of(context).padding.left,
          top: MediaQuery.of(context).padding.top,
          right: MediaQuery.of(context).padding.right,
          bottom: MediaQuery.of(context).padding.bottom),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/background_image.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      alignment: Alignment.bottomCenter,
      child: Stack(
        children: [
          buildWhiteLayer(),
          buildComponents(),
        ],
      ),
    );
  }

  Positioned buildComponents() {
    return Positioned.fill(
      top: kToolbarHeight + 16,
      left: 16,
      right: 16,
      bottom: 16,
      child: BlocBuilder<HomeCubit, HomeState>(
        builder: (context, state) {
          if (state is HomeSuccess) {
            return Column(
              children: [
                VideoPlayerWidget(url: state.careersModel.data.videoUrl),
                ActionButton(
                  text: getTranslate(context, 'description'),
                  icon: Icon(Icons.arrow_circle_down_rounded, color: Colors.white),
                  margin: EdgeInsets.only(top: 20),
                  padding: EdgeInsets.all(12),
                  gradient: LinearGradient(
                    colors: [Color(0xFF478158), Color(0xFF6fc989)],
                  ),
                  fontColor: Colors.white,
                  onTap: () {},
                ),
                buildDescriptionAndGridView(state),
                ActionButton(
                  text: getTranslate(context, 'show_more'),
                  icon: Icon(
                    Icons.arrow_circle_down_rounded,
                    color: Color(0xFF007a82),
                  ),
                  margin: EdgeInsets.only(top: 20),
                  padding: EdgeInsets.all(12),
                  gradient: LinearGradient(
                    colors: [Color(0xFF478158), Color(0xFF6fc989)],
                  ),
                  fontColor: Color(0xFF007a82),
                  onTap: () {},
                ),
              ],
            );
          } else if (state is HomeLoading) {
            setStatusBarColor(Colors.transparent, Brightness.dark);
            return buildLoadingIndicator();
          } else if (state is HomeError) {
            return buildError(state);
          }
          return SizedBox();
        },
      ),
    );
  }

  Widget buildError(HomeError state) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(state.message),
          ElevatedButton(
            onPressed: () {
              homeCubit.getCareers(context);
            },
            child: Text(
              getTranslate(context, 'retry'),
            ),
          ),
        ],
      ),
    );
  }

  Center buildLoadingIndicator() {
    return Center(
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
      ),
    );
  }

  Widget buildWhiteLayer() {
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(height: MediaQuery.of(context).size.height * 0.78, color: Colors.white),
    );
  }

  Widget buildAppBar() {
    return AppBar(
      brightness: Brightness.light,
      backgroundColor: Colors.transparent,
      centerTitle: true,
      elevation: 0,
      title: Image.asset(
        'assets/icons/logo.png',
        width: MediaQuery.of(context).size.width * 0.20,
      ),
      actions: [
        Padding(
          padding: const EdgeInsetsDirectional.only(end: 12.0),
          child: BlocBuilder<HomeCubit, HomeState>(
            builder: (context, state) {
              return LanguageSwitchButton(
                  isActivated: languageApi.language == ENGLISH);
            },
          ),
        ),
      ],
    );
  }

  Widget buildDescriptionAndGridView(HomeSuccess state) {
    var random = Random();
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(top: 16.0),
        child: ScrollConfiguration(
          behavior: MyScrollBehavior(),
          child: SingleChildScrollView(
            padding: const EdgeInsets.only(top: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  state.careersModel.data.description,
                  textAlign: TextAlign.center,
                ),
                GridView.builder(
                  shrinkWrap: true,
                  padding: const EdgeInsets.only(top: 16, bottom: 16),
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 4,
                    crossAxisSpacing: 4,
                  ),
                  itemCount: state.careersModel.data.careersLetters.length,
                  itemBuilder: (context, index) {
                    return GridViewTile(
                      text: state.careersModel.data.careersLetters[index],
                      color: _letters_colors[random.nextInt(4)],
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
