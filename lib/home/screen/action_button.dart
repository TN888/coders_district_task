import 'package:coders_district_task/global/utils/font_sizes.dart';
import 'package:flutter/material.dart';

class ActionButton extends StatelessWidget {
  final String text;

  const ActionButton({
    Key key,
    @required this.text,
    this.icon,
    this.margin = EdgeInsets.zero,
    this.padding = EdgeInsets.zero,
    this.gradient,
    this.color,
    this.fontColor,
    this.onTap,
  })  : assert(text != null),
        assert((gradient != null) != (color != null)),
        super(key: key);
  final Widget icon;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final Gradient gradient;
  final Color color;
  final Color fontColor;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: color,
          gradient: gradient,
          boxShadow: kElevationToShadow[4]),
      child: Center(
        child: Material(
          type: MaterialType.transparency,
          child: InkWell(
            borderRadius: BorderRadius.circular(30),
            child: Padding(
              padding: padding,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    text,
                    style: TextStyle(
                      color: fontColor,
                      fontWeight: FontWeight.bold,
                      fontSize: FontSizes.text,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsetsDirectional.only(start: 8.0),
                    child: icon,
                  ),
                ],
              ),
            ),
            onTap: () {
              onTap();
            },
          ),
        ),
      ),
    );
  }
}
