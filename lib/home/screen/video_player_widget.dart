import 'package:flutter/material.dart';
import 'package:shimmer_animation/shimmer_animation.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerWidget extends StatefulWidget {
  const VideoPlayerWidget({
    Key key,
    @required this.url,
  })  : assert(url != null),
        super(key: key);

  final String url;

  @override
  _VideoPlayerWidgetState createState() => _VideoPlayerWidgetState();
}

class _VideoPlayerWidgetState extends State<VideoPlayerWidget> {
  VideoPlayerController controller;
  Future<void> initializeVideoPlayerFuture;
  bool hideControls;

  @override
  void initState() {
    super.initState();

    controller = VideoPlayerController.network(widget.url);
    initializeVideoPlayerFuture = controller.initialize();
    hideControls = false;
  }

  @override
  void dispose() {
    controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height * 0.30,
      child: FutureBuilder(
        future: initializeVideoPlayerFuture,
        builder: (context, snapshot) {
          return AspectRatio(
            aspectRatio: controller.value.aspectRatio,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                border: Border.all(color: Colors.white, width: 2),
                boxShadow: kElevationToShadow[6],
              ),
              child: snapshot.connectionState == ConnectionState.done
                  ? Stack(
                      alignment: Alignment.center,
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              hideControls = !hideControls;
                            });
                          },
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(30),
                            child: VideoPlayer(controller),
                          ),
                        ),
                        if (!hideControls)
                          FloatingActionButton(
                            backgroundColor: Colors.white,
                            onPressed: () {
                              setState(() {
                                if (controller.value.isPlaying) {
                                  controller.pause();
                                } else {
                                  controller.play();
                                }
                              });
                            },
                            child: Icon(
                              controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
                              color: Colors.green.shade700,
                              size: 32,
                            ),
                          ),
                      ],
                    )
                  : Shimmer(
                      enabled: true,
                      interval: Duration(seconds: 0),
                      child: Container(
                        width: double.infinity,
                        height: MediaQuery.of(context).size.height * 0.30,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: Colors.grey.shade300,

                        ),
                      ),
                    ),
            ),
          );
        },
      ),
    );
  }
}
