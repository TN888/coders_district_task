// To parse this JSON data, do
//
//     final careersModel = careersModelFromJson(jsonString);

import 'dart:convert';

CareersModel careersModelFromJson(String str) => CareersModel.fromJson(json.decode(str));

String careersModelToJson(CareersModel data) => json.encode(data.toJson());

class CareersModel {
  CareersModel({
    this.data,
  });

  final Data data;

  factory CareersModel.fromJson(Map<String, dynamic> json) => CareersModel(
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.videoUrl,
    this.description,
    this.careersLetters,
  });

  final String videoUrl;
  final String description;
  final List<String> careersLetters;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    videoUrl: json["video_url"] == null ? null : json["video_url"],
    description: json["description"] == null ? null : json["description"],
    careersLetters: json["careers_letters"] == null ? null : List<String>.from(json["careers_letters"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "video_url": videoUrl == null ? null : videoUrl,
    "description": description == null ? null : description,
    "careers_letters": careersLetters == null ? null : List<dynamic>.from(careersLetters.map((x) => x)),
  };
}
