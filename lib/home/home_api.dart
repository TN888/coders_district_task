import 'package:coders_district_task/global/apis/base_api.dart';
import 'package:coders_district_task/global/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HomeApi extends BaseApi {
  Future<http.Response> getCareers(BuildContext context, String language) async {
    String url = '$base_url/api/career';

    return await loadData(context, RequestType.get, url, language: language);
  }
}