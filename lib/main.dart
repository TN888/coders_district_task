import 'dart:async';

import 'package:coders_district_task/global/apis/language_api.dart';
import 'package:coders_district_task/global/routes/routes_names.dart';
import 'package:coders_district_task/global/widgets/screen_size_builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'global/localization/localization.dart';
import 'global/routes/routes_generator.dart';
import 'global/utils/font_sizes.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);

  var languageApi = LanguageApi();

  languageApi.getLanguage().then(
    (language) {
      runApp(
        MyApp(languageApi: languageApi),
      );
    },
  );
}

class MyApp extends StatelessWidget {
  MyApp({
    @required this.languageApi,
  }) : assert(languageApi != null);

  final LanguageApi languageApi;

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => languageApi,
      child: StreamBuilder<bool>(
        stream: streamController.stream,
        builder: (context, snapshot) {
          return MaterialApp(
            onGenerateTitle: (context) {
              return getTranslate(context, 'company_name');
            },
            debugShowCheckedModeBanner: false,
            supportedLocales: [
              const Locale(ARABIC),
              const Locale(ENGLISH),
            ],
            locale: Locale(languageApi.language),
            localizationsDelegates: [
              const AppLocalizationsDelegate(),
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              DefaultCupertinoLocalizations.delegate
            ],
            theme: ThemeData(
              primarySwatch: Colors.green,
            ),
            initialRoute: splash,
            onGenerateRoute: RoutesGenerator.allRoutes,
            builder: (context, child) {
              return ScreenSizeBuilder(
                child: child,
                callBack: () {
                  FontSizes.init(ScreenSizeBuilder.size);
                },
              );
            },
          );
        }
      ),
    );
  }
}

StreamController<bool> streamController = StreamController<bool>();

