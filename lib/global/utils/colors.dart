import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const white = Color(0xFFffffff);
  static const transparentWhite = Color(0x66ffffff);
  static const black = Color(0xFF000000);
  static const lightBlack = Color(0xFF53504F);
  static const lightGray = Color(0xFFF0F4F8);
  static const darkGray = Color(0xFF323334);
  static const lightGray2 = Color(0xFF707070);
  static const blue = Color(0xFF29ABE2);
  static const red = Colors.red;
  static const pink = Color(0xFFff4181);
  static const golden = Color(0xFFF1B510);
  static const textDark = Color(0xFF8f8f8f);
  static const green = Color(0xFF39880C);
  static const bgColor = Color(0xFFefefef);
  static const textLight = Color(0xFFc1c2c5);
  static const textLightExtra = Color(0xFFe7e7e7);
  static const yellow = Color(0xFFF1B510);
  static const textBlack = Color(0xFF323334);
  static const transparentGray = Color(0x65323334);
  static const grayLightTransparent = Color(0x34c1c2c3);
  static const orange = Color(0xFFFF5B1D);
  static const darkGreen = Color(0xFF1D512D);
  static const lightOrange = Color(0xFFF99B52);
  static const blueText = Color(0xFFA6BCD0);
  static const transparentOrange = Color(0x55FF5B1D);
  static const lightWhite = Color(0xFFFCFCFC);
  static const customOrange = Color(0xFF3b6f49);
  static const customRed = Color(0xFF9F1818);
  static const lightBlue = Color(0xFFA6BCD0);
  static const lightPink = Color(0xFFFEDCD4);
  static const gray = Colors.grey;
  static const amulet = Color(0xFF789F74);
  static const chathamsBlue = Color(0xFF124975);
  static const hummingBird = Color(0xFFD6EAF8);
  static const customGreen = Color(0xFF3D9F8F);
  static const oceanGreen = Color(0xFF4db580);
}
