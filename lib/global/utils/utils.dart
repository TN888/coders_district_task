import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

setStatusBarColor(Color statusBarColor, Brightness statusBarIconBrightness) {
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: statusBarColor,
      statusBarIconBrightness: statusBarIconBrightness,
    ),
  );
}