import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'colors.dart';

class TextStyles {
  static getWhite(double fontSize) => TextStyle(
        fontSize: fontSize,
        color: AppColors.white,
      );

  static getWhiteBold(double fontSize) => TextStyle(
        fontSize: fontSize,
        color: AppColors.white,
        fontWeight: FontWeight.bold,
      );

  static getWhiteBoldUnderlined(double fontSize) => TextStyle(
        fontSize: fontSize,
        decoration: TextDecoration.underline,
        color: AppColors.white,
        fontWeight: FontWeight.bold,
      );

  static getBlackBold(double fontSize) => TextStyle(
        fontSize: fontSize,
        color: AppColors.black,
        fontWeight: FontWeight.bold,
      );

  static getYellowBold(double fontSize) => TextStyle(
        fontSize: fontSize,
        color: AppColors.yellow,
      );

  static getBlack(double fontSize) => TextStyle(
        fontSize: fontSize,
        color: AppColors.black,
      );

  static getWhiteWithOpacity(double fontSize) => TextStyle(
        fontSize: fontSize,
        color: AppColors.white.withOpacity(0.5),
      );

  static getGreen(double fontSize) => TextStyle(
        fontSize: fontSize,
        color: AppColors.green,
      );

  static getBlackThin(double fontSize) => TextStyle(
        fontSize: fontSize,
        color: AppColors.black,
        fontWeight: FontWeight.w500,
      );

  static getLightBlackBold(double fontSize) => TextStyle(
        fontSize: fontSize,
        color: AppColors.lightBlack,
        fontWeight: FontWeight.bold,
      );

  static getLightBlack(double fontSize) => TextStyle(
        fontSize: fontSize,
        color: AppColors.lightBlack,
      );

  static getErrorStyle(double fontSize) => TextStyle(
        fontSize: fontSize,
        color: AppColors.red,
      );
}
