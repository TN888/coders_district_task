import '../widgets/screen_size_builder.dart';

class FontSizes {
  static double title;
  static double subtitle;
  static double bigText;
  static double text;
  static double smallText;
  static double notice;

  static void init(ScreenSizes size) {
    if (size == ScreenSizes.extraLarge) {
      title = 39.0;
      subtitle = 32.5;
      bigText = 31.5;
      text = 28.5;
      smallText = 24.5;
      notice = 20.0;
    } else if (size == ScreenSizes.large) {
      title = 37.0;
      subtitle = 28.0;
      bigText = 26.0;
      text = 24.0;
      smallText = 21.5;
      notice = 19.0;
    } else if (size == ScreenSizes.medium) {
      title = 33.0;
      subtitle = 25.0;
      bigText = 23.5;
      text = 21.5;
      smallText = 19.5;
      notice = 17.5;
    } else if (size == ScreenSizes.small) {
      title = 31.0;
      subtitle = 24.0;
      bigText = 22.5;
      text = 20.0;
      smallText = 18.5;
      notice = 15.0;
    } else if (size == ScreenSizes.extraSmall) {
      title = 29.0;
      subtitle = 21.0;
      bigText = 20.0;
      text = 19.0;
      smallText = 17.5;
      notice = 14.5;
    } else {
      title = 26.0;
      subtitle = 18.0;
      bigText = 17.0;
      text = 16.0;
      smallText = 15.0;
      notice = 13.0;
    }
  }
}
