// To parse this JSON data, do
//
//     final messageResponseModel = messageResponseModelFromJson(jsonString);

import 'dart:convert';

MessageResponseModel messageResponseModelFromJson(String str) => MessageResponseModel.fromJson(json.decode(str));

String messageResponseModelToJson(MessageResponseModel data) => json.encode(data.toJson());

class MessageResponseModel {
  MessageResponseModel({
    this.message,
  });

  final String message;

  factory MessageResponseModel.fromJson(Map<String, dynamic> json) => MessageResponseModel(
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "message": message == null ? null : message,
  };
}
