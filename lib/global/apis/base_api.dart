import 'dart:convert';
import 'dart:io';

import 'package:coders_district_task/global/localization/localization.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'language_api.dart';

enum RequestType { get, post }

class BaseApi {
  loadData(BuildContext context, RequestType requestType, String url,
      {String language = ENGLISH, Map body}) async {
    try {
      http.Response response;
      if (requestType == RequestType.get) {
        response = await _getRequest(url, headers: _setHeaders(language));
      } else {
        response = await _postRequest(url, body, headers: _setHeaders(language));
      }

      return response;
    } on SocketException {
      throw Exception(getTranslate(context, 'no_connection'));
    }
  }

  _getRequest(String url, {Map<String, String> headers}) async {
    final response = await http.get(url, headers: headers);
    return response;
  }

  _postRequest(String url, Map body, {Map<String, String> headers}) async {
    final response = await http.post(url, headers: headers, body: json.encode(body));
    return response;
  }

  _setHeaders(String language) {
    var headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    };

    if (language != null) {
      headers['lang'] = language;
    }
    return headers;
  }
}
