import 'package:shared_preferences/shared_preferences.dart';

const LANGUAGE_KEY = 'language key';

const ENGLISH = 'en';
const ARABIC = 'ar';

class LanguageApi {
  String _language;

  String defaultLanguage = ENGLISH;

  Future<bool> setLanguage(String language) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    bool result = await prefs.setString(LANGUAGE_KEY, language);

    if (result) _language = language;
    return result;
  }

  Future<String> getLanguage() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    _language = prefs.getString(LANGUAGE_KEY) ?? defaultLanguage;
    return _language;
  }

  String get language => _language;
}