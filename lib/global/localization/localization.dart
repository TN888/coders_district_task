import 'dart:async';
import 'dart:convert';

import 'package:coders_district_task/global/apis/language_api.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

String getTranslate(BuildContext context, String key) {
  return Localization.of(context).trans(key);
}

class Localization {
  Localization(this.locale);

  final Locale locale;

  static Localization of(BuildContext context) {
    return Localizations.of<Localization>(context, Localization);
  }

  Map<String, String> _sentences;

  Future<bool> load() async {
    String data = await rootBundle.loadString(_setTranslationsPath(this.locale.languageCode));
    Map<String, dynamic> _result = json.decode(data);

    this._sentences = new Map();
    _result.forEach((String key, dynamic value) {
      this._sentences[key] = value.toString();
    });
    return true;
  }

  String trans(String key) {
    return this._sentences[key] ?? '';
  }

  String _setTranslationsPath(String languageCode) {
    return 'assets/lang/$languageCode/translations.json';
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<Localization> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => [ARABIC, ENGLISH].contains(locale.languageCode);

  @override
  Future<Localization> load(Locale locale) async {
    Localization localizations = new Localization(locale);
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}
