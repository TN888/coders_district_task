import 'package:coders_district_task/global/localization/localization.dart';
import 'package:coders_district_task/home/cubit/home_cubit.dart';
import 'package:coders_district_task/home/home_api.dart';
import 'package:coders_district_task/home/screen/home_screen.dart';
import 'package:coders_district_task/splash/splash_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'routes_names.dart';

class RoutesGenerator {
  RoutesGenerator._();

  static Route<dynamic> allRoutes(RouteSettings settings) {
    switch (settings.name) {
      case home:
        return MaterialPageRoute(
          builder: (context) {
            return BlocProvider(
              create: (context) => HomeCubit(
                homeApi: HomeApi(),
              ),
              child: HomeScreen(),
            );
          },
        );
      case splash:
        return MaterialPageRoute(
          builder: (context) => SplashScreen(),
        );
      default:
        return MaterialPageRoute(
          builder: (context) => Center(
            child: Text(
              getTranslate(context, 'try_again'),
            ),
          ),
        );
    }
  }
}
