import 'package:flutter/material.dart';

class ErrorText extends StatelessWidget {
  final String message;
  final double fontSize;
  final Color color;
  final double height;

  ErrorText(this.message, this.fontSize, this.color, this.height);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(24),
      height: height ?? MediaQuery.of(context).size.height,
      alignment: Alignment.center,
      child: Text(
        message,
        style: TextStyle(
          color: color,
          fontSize: fontSize,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
}
